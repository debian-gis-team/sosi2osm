sosi2osm (1.0.0-8) UNRELEASED; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.0, no changes.
  * Update watch file for GitHub URL changes.
  * Bump debhelper compat to 12, no changes.
  * Update lintian overrides.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 28 Nov 2020 14:24:50 +0100

sosi2osm (1.0.0-7) unstable; urgency=medium

  * Team upload.
  * Add patch by Helmut Grohne to fix FTCBFS.
    (closes: #932065)
  * Bump Standards-Version to 4.5.0, no changes.
  * Drop Name field from upstream metadata.
  * Bump watch file version to 4.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 12 Nov 2020 16:13:28 +0100

sosi2osm (1.0.0-6) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.4.0, no changes.
  * Define ACCEPT_USE_OF_DEPRECATED_PROJ_API_H for PROJ 6.0.0 compatibility.
  * Update gbp.conf to use --source-only-changes by default.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 11 Jul 2019 11:24:04 +0200

sosi2osm (1.0.0-5) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.2.1, no changes.
  * Update watch file to handle common issues.

  [ Ruben Undheim ]
  * debian/compat: level 11
  * debian/control: debhelper >= 11
  * debian/patches: Refreshed all patches using 'gbp pq'
  * debian/tests: added autopkgtest
  * Remove lintian override for testsuite-autopkgtest-missing.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 22 Sep 2018 15:37:13 +0200

sosi2osm (1.0.0-4) unstable; urgency=medium

  * Team upload.
  * Add upstream metadata.
  * Strip trailing whitespace from rules file.
  * Update copyright-format URL to use HTTPS.
  * Bump Standards-Version to 4.1.5, no changes.
  * Add patch to fix spelling errors.
  * Update Vcs-* URLs for Salsa.
  * Add lintian override for testsuite-autopkgtest-missing.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 01 Aug 2018 19:46:12 +0200

sosi2osm (1.0.0-3) unstable; urgency=medium

  * Team upload.
  * Update Vcs-Git URL to use HTTPS.
  * Add patch to use std::max from algorithm.
    (closes: #835717)
  * Use standalone license paragraph for GPL-3+.
  * Bump Standards-Version to 3.9.8, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 01 Sep 2016 12:22:00 +0200

sosi2osm (1.0.0-2) unstable; urgency=low

  * debian/rules:
    - Enabled hardening=+all
  * debian/sosi2osm.1:
    - Do not list sosi2osm.lua in man-page
  * Added debian/gbp.conf to automatically enable pristine-tar build

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 23 May 2015 13:17:52 +0200

sosi2osm (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #760597)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 13 Oct 2014 21:27:02 +0200
